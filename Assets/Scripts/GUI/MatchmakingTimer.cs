﻿using TMPro;
using UnityEngine;

public class MatchmakingTimer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI timeQueue;
    private int Minutes;
    private float Seconds;

    private void OnEnable()
    {
        Minutes = 0;
        Seconds = 0;
    }

    // Update is called once per frame

    private void FixedUpdate()
    {

        timeQueue.text = timeToString();

        Seconds += Time.fixedDeltaTime;
    }

    private string timeToString()
    {
        string SecondString = "";

        if (Seconds >= 60)
        {
            Seconds = 0;
            Minutes++;
        }

        if(Seconds < 10)
        {
            SecondString += "0";
        }


        SecondString += (int)Seconds;


        return Minutes + ":" + SecondString;
    }
}
