﻿
using UnityEngine;
using Photon.Pun;

public class GameplayCanvas : MonoBehaviourPun
{

    public void Disconnect()
    {
        NetworkManager.instance.LeaveRoom();
    }
}
