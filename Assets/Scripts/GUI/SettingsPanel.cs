﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanel : MonoBehaviour
{
    [SerializeField] Slider mouseSensitSlider;



    private void Awake()
    {
        mouseSensitSlider.value = SettingsManager.Instance.cameraMouseSensit;
    }

    public void SetMouseSensit()
    {
        if(SettingsManager.Instance)
        {
            SettingsManager.Instance.cameraMouseSensit = mouseSensitSlider.value;
        }
    }
}
