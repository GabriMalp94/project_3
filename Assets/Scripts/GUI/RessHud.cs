﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RessHud : MonoBehaviour
{
    [SerializeField] private Image fillBar;


    public void SetFillBar(float ammount)
    {
        fillBar.fillAmount = ammount;
    }
}
