﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGameHud : MonoBehaviour
{
    [SerializeField] private GameObject winImage;
    [SerializeField] private GameObject loseImage;
    [SerializeField] private GameObject winScenicObj;
    [SerializeField] private GameObject loseScenicObj;

    private void Awake()
    {
        Debug.Log(EndGame.Instance.CurrentStatus);
        switch(EndGame.Instance.CurrentStatus)
        {
            case Gamestatus.win:
                winImage.SetActive(true);
                loseImage.SetActive(false);
                Instantiate(winScenicObj);
                break;

            case Gamestatus.gameOver:
                winImage.SetActive(false);
                loseImage.SetActive(true);
                Instantiate(loseScenicObj);
                break;
        }
    }

    public void LeaveRoom()
    {
        NetworkManager.instance.LeaveRoom();
    }

    public void Quit()
    {
        NetworkManager.instance.Quit();
    }
}
