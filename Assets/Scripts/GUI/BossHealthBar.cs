﻿using UnityEngine;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{
    [SerializeField] Image HealthFillBar;

    public void HealthBarUpdate(float _fillAmmount)
    {
        HealthFillBar.fillAmount = _fillAmmount;
    }
}
