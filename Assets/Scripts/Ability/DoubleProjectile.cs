﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;

public class DoubleProjectile : Projectile
{

    [SerializeField] float rotSeed;

    float rotZ;

    protected override void OnAwake()
    {
        rotZ = transform.eulerAngles.z;

        base.OnAwake();
    }
    // Update is called once per fram

    protected override void OnUpdate()
    {
        rotZ += Time.deltaTime * rotSeed;
        transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, rotZ);
        base.OnUpdate();
    }
}
