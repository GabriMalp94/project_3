﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityController : MonoBehaviour
{
	protected PhotonView _photonView;
	protected List<Ability> entityAbilities = new List<Ability>();

	private void Awake()
	{
		init();
	}

	protected virtual void init()
	{
		_photonView = GetComponent<PhotonView>();
	}
}
