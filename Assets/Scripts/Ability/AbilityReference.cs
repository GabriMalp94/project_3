﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AbilityReference : MonoBehaviour
{
	public static AbilityReference instance;

	[SerializeField] private List<BossAbility> _bossAbilites;
	public List<BossAbility> BossAbilities { get { return _bossAbilites; } }
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(this.gameObject);
		}
	}

	public Ability GetMaskAbility(MaskType _maskType)
	{
        if(_maskType == MaskType.none)
        {
            Debug.Log("no spell");
            return null;
        }
		BossAbility ability = _bossAbilites.First((x) => x.CurrentMaskType.Equals(_maskType));
		return Instantiate(ability.CurrentAbility);
	}
}
