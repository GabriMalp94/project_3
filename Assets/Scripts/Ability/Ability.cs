﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public delegate void SpellDelegate();

public class Ability : ScriptableObject
{
    [SerializeField] private string _spellName;
	public string SpellName { get { return _spellName; } }

	[SerializeField] protected GameObject SpellVFX;

    public bool OnDeathCastIstant = false;

	public virtual void spellEffect()
	{
	
	}

    public virtual void OnDeathSpellEffect()
    {

    }
}
