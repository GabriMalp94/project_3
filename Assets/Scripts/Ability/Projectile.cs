﻿using Photon.Pun;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] protected float damage;
    [SerializeField] protected float speed;
    [SerializeField] protected GameObject VFX_Hit;

    PhotonView _photonView;

    private void Awake()
    {
        OnAwake();
    }

    protected virtual void OnAwake()
    {
        _photonView = GetComponent<PhotonView>();
    }

    public void setRotation(Vector3 pos)
    {
        if(_photonView.IsMine)
        {

            Vector3 distance = pos - transform.position;
            Quaternion rotate = Quaternion.LookRotation(distance);
            transform.rotation = rotate;
        }
    }



    private void LateUpdate()
    {
        if(_photonView.IsMine)
        {
            OnUpdate();
        }
    }


    protected virtual void OnUpdate()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
       
        if(_photonView.IsMine && !other.gameObject.TryGetComponent<PlayerController>(out PlayerController currentPlayer) && !other.gameObject.TryGetComponent<BossContoller>(out BossContoller currentBossTrigger))
        {
            if(other.gameObject.TryGetComponent(out Mask _mask))
            {
                _mask.TakeDamage(damage);
            }
            else if(other.gameObject.TryGetComponent<BossAdds>(out BossAdds currentAdd))
            {
                currentAdd.TakeDamage(damage);
            }

            if(other.tag != "Ignore")
            {
                GameManager.instance.NetInstance(VFX_Hit.name, transform.position, Quaternion.identity);
                PhotonNetwork.Destroy(this.gameObject);
            }
        }
    }
}
