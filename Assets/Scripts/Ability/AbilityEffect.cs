﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityEffect : MonoBehaviour
{
    protected PhotonView _photonView;


    private void Start()
    {
        StartCoroutine(AbilityCoroutine());
    }

    protected virtual IEnumerator AbilityCoroutine()
    {
        yield return null;
    }
}
