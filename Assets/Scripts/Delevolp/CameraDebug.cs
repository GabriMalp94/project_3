﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDebug : MonoBehaviour
{

    [SerializeField] float maxVerticalAngle = 60;
    [SerializeField] float minVerticalAngle = -60;
    [SerializeField] float CameraSensit;

    private float CameraRotX;
    private float PlayerRotY;

    Camera currentCamera;
    [SerializeField] Projectile Spell_VFX;
    [SerializeField] Transform SpellCastPoint;

    private void Awake()
    {
        currentCamera = GetComponent<Camera>();
    }
    // Start is called before the first frame update
    void Start()
    {
        PlayerRotY = transform.eulerAngles.y;
        CameraRotX = transform.eulerAngles.x;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        MoveCamera();
        SpellCast();
    }

    private void MoveCamera()
    {

        CameraRotX -= Input.GetAxis("Mouse Y") * CameraSensit * Time.deltaTime;
        CameraRotX = Mathf.Clamp(CameraRotX, minVerticalAngle, maxVerticalAngle);

        PlayerRotY += Input.GetAxis("Mouse X") * CameraSensit * Time.deltaTime;

        Quaternion rotation = Quaternion.Euler(CameraRotX, PlayerRotY, 0);
        transform.localRotation = rotation;
    }

    private void SpellCast()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
        {
            Ray currentRay = currentCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit;

            Projectile currentProjectile = Instantiate(Spell_VFX, SpellCastPoint.position, SpellCastPoint.rotation);

            if(Physics.Raycast(currentRay, out hit))
            {
                Debug.Log(hit.collider.name);
                currentProjectile.setRotation(hit.point);
            }
        }
    }

}
