﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Photon.Pun;

[RequireComponent(typeof(PhotonView))]
public class ShatterableMask : MonoBehaviour
{
    protected PhotonView photonView;
    public PhotonView MaskPhotonView { get { return photonView; } }

    [SerializeField] protected MaskType maskType;

    [SerializeField] float _maxHealth = 100;

    public float MaxHealth { get { return _maxHealth; } }

    protected float _currentHealth;
    public float CurrentHealth { get { return _currentHealth; } }

    [SerializeField] GameObject maskContainer;
    protected List<MeshRenderer> _meshrendererList;
    private List<Rigidbody> shatterRigidbody;
    private List<BoxCollider> boxColliderList;
    private BoxCollider maskCollider;


    private ExplosionShard explosionshard;
    private Rigidbody explosionRb;

    [ColorUsage(true, true)]
    [SerializeField] Color emissionColor;

    [SerializeField] float dissolveSpeed;

    protected bool isDead = false;
    public bool IsDead { get { return isDead; } }

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();

        _currentHealth = _maxHealth;

        _meshrendererList = maskContainer.GetComponentsInChildren<MeshRenderer>().ToList();
        boxColliderList = maskContainer.GetComponentsInChildren<BoxCollider>().ToList();
        shatterRigidbody = maskContainer.GetComponentsInChildren<Rigidbody>().ToList();

        explosionshard = GetComponentInChildren<ExplosionShard>();
        explosionRb = explosionshard.gameObject.GetComponent<Rigidbody>();

        explosionshard.SetList(shatterRigidbody);

        maskCollider = GetComponent<BoxCollider>();
        SetCollider(false);
        SetShatterRb(false);

        customAwake();

        foreach (var mr in _meshrendererList)
        {
            mr.material.SetColor("_emissionColor", emissionColor);
            mr.material.SetFloat("_dissolve", 1);
        }

    }

    protected virtual void customAwake()
    {

    }


    public void TakeDamage(float _damage)
    {
        if (_currentHealth > 0)
        {
            TakeDamageRPC(_damage);
            photonView.RPC("TakeDamageRPC", RpcTarget.Others, _damage);
        }
    }

    [PunRPC]
    protected void TakeDamageRPC(float _damage)
    {
        _currentHealth -= _damage;
        BossContoller.instance.RefreshHp();
        Debug.LogError( gameObject.name +" damage " + _damage + " current health: " + _currentHealth);
        if (_currentHealth <= 0 && !isDead)
        {
            Debug.LogError("mask death");
            _currentHealth = 0;
            isDead = true;
            transform.parent = null;
            SetCollider(true);
            SetShatterRb(true);
            onDeath();
            BossContoller.instance.OnMaskDeath();
            if (PhotonNetwork.IsMasterClient && maskType != MaskType.none)
            {
                BossContoller.instance.OnDeathMask(maskType);
            }
        }

    }

    protected virtual void onDeath()
    {

    }

    public void HealMask(float _ammount)
    {

        if (_currentHealth < _maxHealth)
        {
            HealRPC(_ammount);
            photonView.RPC("HealRPC", RpcTarget.Others, _ammount);
        }
    }

    [PunRPC]
    protected void HealRPC(float _ammount)
    {
        _currentHealth += _ammount;
        Debug.LogError("mask healed: " + _ammount);
        if (_currentHealth > _maxHealth)
        {
            _currentHealth = _maxHealth;
        }

        BossContoller.instance.RefreshHp();
    }

    public void SetShader()
    {
        StartCoroutine(dissolve());
    }

    protected IEnumerator dissolve()
    {
        float currentDissolve = 1f;
        while (currentDissolve > 0)
        {
            currentDissolve -= Time.deltaTime * dissolveSpeed;
            foreach (var mr in _meshrendererList)
            {
                mr.material.SetFloat("_dissolve", currentDissolve);
            }
            yield return null;
        }

        yield return null;
    }

    protected void SetCollider(bool visible)
    {
        foreach (var box in boxColliderList)
        {
            if (box != maskCollider)
            {
                box.enabled = visible;
            }
            else
            {
                box.enabled = !visible;
            }
        }


    }

    protected void SetShatterRb(bool active)
    {
        foreach (var rb in shatterRigidbody)
        {
            rb.useGravity = active;
        }
        explosionRb.useGravity = active;
    }

}
