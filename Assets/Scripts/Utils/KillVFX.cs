﻿using UnityEngine;

public class KillVFX : MonoBehaviour
{
    [SerializeField] float destroyTime;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(this.gameObject, destroyTime);   
    }

}
