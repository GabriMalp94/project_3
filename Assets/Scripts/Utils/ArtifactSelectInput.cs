﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ArtifactSelectInput 
{
    [SerializeField] private KeyCode artifactKeycode;
    public KeyCode ArtifactKeycode { get { return artifactKeycode; } }

    [SerializeField] private int artifactIndex;
    public int ArtifactIndex { get { return artifactIndex; } }
}
