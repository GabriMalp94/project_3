﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class ShieldHitter : MonoBehaviour
{
    private PhotonView currentPhotonView;
    public PhotonView CurrentPhotonView { get { return currentPhotonView;} }

    private void Awake()
    {
        currentPhotonView = GetComponent<PhotonView>();
    }
}
