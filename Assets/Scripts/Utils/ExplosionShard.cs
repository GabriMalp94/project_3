﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionShard : MonoBehaviour
{
    List<Rigidbody> shardRbList = new List<Rigidbody>();

    [SerializeField] private float maxForce;
    [SerializeField] private float minForce;

    [SerializeField] private float radius;

    [SerializeField] GameObject ExplosionVFX;
    GameObject currentExplosion;

    public void SetList(List<Rigidbody> _shardRb)
    {
        shardRbList = _shardRb;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Ground")
        {
            if(!currentExplosion && ExplosionVFX)
            {
                currentExplosion = Instantiate(ExplosionVFX, transform.position, Quaternion.identity);
                BossContoller.instance.MaskDeath_SFX();

            }
            Destroy(this.gameObject);
        }
    }
}
