﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveSelectionScene : MonoBehaviour
{
    [SerializeField] private Transform startPosition;
    [SerializeField] private Transform LobbyPosition;
    [SerializeField] private Transform MatchMakingPosition;
    [SerializeField] private Transform HowToPlayPosition;
    [SerializeField] private Transform LorePosition;

    [SerializeField] private float CameraSpeed;
    [SerializeField] private float travelTime;
    private static CameraMoveSelectionScene instance;
    public static CameraMoveSelectionScene Instance { get { return instance; } }

    private void Awake()
    {
        instance = this;
    }

    public void ChangeCameraPosition(NetworkStatus _status)
    {
        switch(_status)
        {
            case NetworkStatus.offline:
                StopAllCoroutines();
                StartCoroutine(ChangePosition(startPosition));
                break;

            case NetworkStatus.howToPlay:
                StopAllCoroutines();
                StartCoroutine(ChangePosition(HowToPlayPosition));
                break;

            case NetworkStatus.connectedLobby:
                StopAllCoroutines();
                StartCoroutine(ChangePosition(LobbyPosition));
                break;

            case NetworkStatus.ConnectedRoom:
                StopAllCoroutines();
                StartCoroutine(ChangePosition(MatchMakingPosition));
                break;

            case NetworkStatus.lore:
                StopAllCoroutines();
                StartCoroutine(ChangePosition(LorePosition));
                break;

        }
    }

    IEnumerator ChangePosition(Transform position)
    {
        float currentTime = 0;
        while(currentTime < travelTime)
        {
            transform.position = Vector3.Slerp(transform.position, position.position, currentTime * CameraSpeed);
            transform.rotation = Quaternion.Slerp(transform.rotation, position.rotation, currentTime * CameraSpeed);
            currentTime += Time.deltaTime;
            yield return null;
        }
        yield return null;
    }

    private void Update()
    {
        if(NetworkManager.instance._CurrentnetworkStatus == NetworkStatus.howToPlay || NetworkManager.instance._CurrentnetworkStatus == NetworkStatus.lore)
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                NetworkManager.instance.ChangeNetStatus(NetworkStatus.offline);
            }
        }
    }
}
