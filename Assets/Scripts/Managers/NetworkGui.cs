﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;

public class NetworkGui : MonoBehaviour
{
    public static NetworkGui instance;

    [SerializeField] TextMeshProUGUI NetworkMessage;
    [SerializeField] GameObject StartButtons;
    [SerializeField] GameObject LobbyButtons;
    [SerializeField] GameObject MatchmakingButtons;
    [SerializeField] GameObject QuitBtn;
    [SerializeField] GameObject startingDungeonLabel;


    [SerializeField] private List<Transform> playerPreview;
    [SerializeField] private List<GameObject> avatars;
    private List<GameObject> instantiatedAvatars = new List<GameObject>();

    private void Awake()
    {
        instance = this;
    }

    public void NetworkPanelUpdate(NetworkStatus _status)
    {
        if (SceneManager.GetActiveScene().buildIndex != 0)
            return;

        switch(_status)
        {
            case NetworkStatus.offline:
                StartButtons.SetActive(true);
                LobbyButtons.SetActive(false);
                MatchmakingButtons.SetActive(false);
                QuitBtn.SetActive(true);
                startingDungeonLabel.SetActive(false);

                break;

            case NetworkStatus.howToPlay:
                StartButtons.SetActive(false);
                LobbyButtons.SetActive(false);
                MatchmakingButtons.SetActive(false);
                QuitBtn.SetActive(true);
                startingDungeonLabel.SetActive(false);

                break;

            case NetworkStatus.lore:
                StartButtons.SetActive(false);
                LobbyButtons.SetActive(false);
                MatchmakingButtons.SetActive(false);
                QuitBtn.SetActive(true);
                startingDungeonLabel.SetActive(false);

                break;

            case NetworkStatus.linking:
                StartButtons.SetActive(false);
                LobbyButtons.SetActive(false);
                MatchmakingButtons.SetActive(false);
                QuitBtn.SetActive(false);
                startingDungeonLabel.SetActive(false);

                break;

            case NetworkStatus.connectedLobby:
                StartButtons.SetActive(false);
                LobbyButtons.SetActive(true);
                MatchmakingButtons.SetActive(false);
                QuitBtn.SetActive(true);
                startingDungeonLabel.SetActive(false);

                break;

            case NetworkStatus.ConnectedRoom:
                StartButtons.SetActive(false);
                LobbyButtons.SetActive(false);
                MatchmakingButtons.SetActive(true);
                startingDungeonLabel.SetActive(false);

                break;

            case NetworkStatus.dungeonStarting:
                StartButtons.SetActive(false);
                LobbyButtons.SetActive(false);
                MatchmakingButtons.SetActive(false);
                startingDungeonLabel.SetActive(true);

                break;

        }
    }

    public void NetTextUpdate(string _message)
    {
        NetworkMessage.text = _message;
    }

    public void OnClickMasterConnect()
    {
        NetworkManager.instance.Connect();
    }

    public void OnClickDisconnect()
    {
        NetworkManager.instance.Disconnect();
    }

    public void OnClickMatchmaking()
    {
        NetworkManager.instance.MatchMaking();
    }

    public void OnClickLeaveQueue()
    {
        NetworkManager.instance.LeaveQueue();
    }

    public void OnClickQuit()
    {
        NetworkManager.instance.Quit();
    }

    public void SetPreviewAvatar()
    {
        foreach (var a in instantiatedAvatars)
        {
            Destroy(a);
        }
        instantiatedAvatars.Clear();

        Debug.Log(PhotonNetwork.PlayerList.Length);
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            GameObject currentAvatar = Instantiate(avatars[i]);
            currentAvatar.transform.position = playerPreview[i].position;
            currentAvatar.transform.rotation = playerPreview[i].rotation;
            instantiatedAvatars.Add(currentAvatar);
        }
    }

    public void HowToPlay()
    {
        NetworkManager.instance.ChangeNetStatus(NetworkStatus.howToPlay);
    }

    public void OnClickLore()
    {
        NetworkManager.instance.ChangeNetStatus(NetworkStatus.lore);
    }
}
