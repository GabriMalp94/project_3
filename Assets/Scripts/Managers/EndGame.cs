﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    private Gamestatus currentStatus;
    public Gamestatus CurrentStatus { get { return currentStatus; } }

    private static EndGame instance;
    public static EndGame Instance { get { return instance; } }

    public void setEndGame(Gamestatus _status)
    {
        currentStatus = _status;
        DontDestroyOnLoad(this);
        instance = this;
        SceneManager.LoadScene(2);
    }

}
