﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private static SoundManager instance;
    public static SoundManager Instance { get { return instance; } }

    private AudioSource globalAudiosource;

    [SerializeField] private AudioClip SFX_background_Music;
    public AudioClip SFX_Background_Music { get { return SFX_background_Music; } }

    [SerializeField] private AudioClip SFX_player_Step;
    public AudioClip SFX_Player_Step { get { return SFX_player_Step; } }

    [SerializeField] private AudioClip SFX_boss_Scream;
    public AudioClip SFX_Boss_Scream { get { return SFX_boss_Scream; } }

    [SerializeField] private AudioClip SFX_artifact_Activation;
    public AudioClip SFX_Artifact_Activation { get { return SFX_artifact_Activation; } }

    [SerializeField] private AudioClip SFX_artifact_Monkey;
    public AudioClip SFX_Artifact_Monkey { get { return SFX_artifact_Monkey; } }

    [SerializeField] private AudioClip SFX_basic_Cast;
    public AudioClip SFX_Basoc_Cast { get { return SFX_basic_Cast; } }

    [SerializeField] private AudioClip SFX_mask_death;
    public AudioClip SFX_Mask_Death { get { return SFX_mask_death; } }

    [SerializeField] private AudioClip SFX_fire_columns;
    public AudioClip SFX_Fire_Columns { get { return SFX_fire_columns; } }

    [SerializeField] private AudioClip SFX_water_splash;
    public AudioClip SFX_Water_Spalsh { get { return SFX_Water_Spalsh; } }

    [SerializeField] private AudioClip SFX_loopMusic;

    [SerializeField] private AudioClip SFX_last_mask;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        globalAudiosource = GetComponent<AudioSource>();
    }

    private void StartBackGround(AudioClip _music)
    {
        globalAudiosource.clip = _music;
        globalAudiosource.Play();
    }

    public void TriggerBackGroundMusic()
    {
        StartBackGround(SFX_background_Music);
        StartCoroutine(changeMusiconLoop());
    }

    public void LastMaskMusic()
    {
        StartBackGround(SFX_last_mask);
    }

    IEnumerator changeMusiconLoop()
    {
        yield return new WaitForSeconds(SFX_background_Music.length);
        if(globalAudiosource.clip != SFX_last_mask)
        {
            StartBackGround(SFX_loopMusic);
        }
        yield return null;
    }
}
