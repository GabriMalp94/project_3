﻿using UnityEngine;

public class PrefabManager : MonoBehaviour
{
    public static PrefabManager instance;

    [SerializeField] PlayerController _playerPrefab;
    public PlayerController PlayerPrefab { get { return _playerPrefab; } }

    [SerializeField] AvatarController[] _avatarsPrefab;
    public AvatarController[] AvatarsPrefab { get { return _avatarsPrefab; } }
 
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
