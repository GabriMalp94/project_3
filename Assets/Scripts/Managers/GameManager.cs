﻿using UnityEngine;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;


public enum Gamestatus
{
    running,
    gameOver,
    win,
}

public class GameManager : MonoBehaviourPun
{
    public static GameManager instance;
    private PhotonView _photonView;
    public PhotonView ManagerPhotonView { get { return _photonView; } }

    private PlayerController _localPlayer;
    public PlayerController LocalPlayer { get { return _localPlayer; } }

    [SerializeField] private BossContoller _bossController;
    public BossContoller CurrentBossController { get { return _bossController; } }

    [SerializeField] Transform[] SpawnPoints;

    private List<PlayerController> _playerList = new List<PlayerController>();
    public List<PlayerController> PlayerList { get { return _playerList; } }

    private Gamestatus currentGamestatus;
    public Gamestatus CurrentGameStatus { get { return currentGamestatus; } }

    [SerializeField] EndGame endgamePrefab;

    public bool offlineMode = false;

    [SerializeField] private float OnDefeatDelay = 2;
    [SerializeField] private float OnWinDelay = 2;

    public bool IsOptionOpen { get; set; }

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        _photonView = GetComponent<PhotonView>();

        HideCursor();

        currentGamestatus = Gamestatus.running;
        PhotonNetwork.OfflineMode = offlineMode;
        Debug.LogError("Offline Mode: " + offlineMode);

        Transform currentSpawnPoint = PhotonNetwork.IsMasterClient ? SpawnPoints[0] : SpawnPoints[1];
        _localPlayer = PhotonNetwork.Instantiate(PrefabManager.instance.PlayerPrefab.name, currentSpawnPoint.position, currentSpawnPoint.rotation).GetComponent<PlayerController>();
        _playerList.Add(_localPlayer);

        if (PhotonNetwork.IsMasterClient)
        {
            _localPlayer.SetPlayerID(0);
        }
        else
        {
            _localPlayer.SetPlayerID(1);
        }
    }

    private void Start()
    {
      
    }

    public void SpawnedPlayer(int _viewID)
    {
        _photonView.RPC("joinedPlayer", RpcTarget.Others, _viewID);
    }

    [PunRPC]
    private void joinedPlayer(int _viewID)
    {
        _playerList.Add((PhotonNetwork.GetPhotonView(_viewID).gameObject).GetComponent<PlayerController>());
        Debug.LogError("player add: " + _viewID);
    }

    public void HideCursor()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void ShowCursor()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

	public GameObject NetInstance(string _name, Vector3 pos, Quaternion rot)
	{
		return PhotonNetwork.Instantiate(_name, pos, rot);
	}

    public void OnPlayerDeath()
    {
        if(currentGamestatus == Gamestatus.running)
        {
            StartCoroutine(loseCheck());
        }
    }

    IEnumerator loseCheck()
    {
        bool gameover = true;
        foreach (var p in _playerList)
        {
            if (p.CurrentPlayerStatus.Equals(PlayerStatus.Alive))
            {
                gameover = false;
                break;
            }
        }

        if (gameover)
        {
            yield return new WaitForSeconds(OnDefeatDelay);
            SetEndGame(Gamestatus.gameOver);
        }
        yield return null;
    }

    public void SetEndGame(Gamestatus _status)
    {
        currentGamestatus = _status;
        ShowCursor();
        EndGame currentEndGame = Instantiate(endgamePrefab);
        currentEndGame.setEndGame(_status);
    }

    public void VictoryCheck()
    {
        StartCoroutine(winConroutine());
    }

    IEnumerator winConroutine()
    {
        yield return new WaitForSeconds(OnWinDelay);
        checkMaskAlive();
        yield return null;
    }

    private void checkMaskAlive()
    {
        bool bossDeafeat = true;
        foreach(var m in BossContoller.instance.Masks)
        {
            Debug.Log(m.IsDead);
            if(!m.IsDead)
            {
                bossDeafeat = false;
                break;
            }
        }
        if(bossDeafeat)
        {
            SetEndGame(Gamestatus.win);
        }
    }
}
