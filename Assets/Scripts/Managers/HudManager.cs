﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HudManager : MonoBehaviour
{
    public static HudManager instance;
    [SerializeField] private BossHealthBar bossHealthBar;

    [SerializeField] private Image artifactImage;
    [SerializeField] private Image manaBar;

    [SerializeField] private Color fullHealthColor;
    [SerializeField] private Color lowHealthColor;
    [SerializeField] private Image hpBar;

    [SerializeField] private GameObject DeathPanel;
    [SerializeField] private RessHud ressHud;
    [SerializeField] private GameObject optionPanel;
    [SerializeField] private GameObject PlayerHudOverlay;
    [SerializeField] private GameObject settingPanel;

    [SerializeField] private Image damageOverlay;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        bossHealthBar.gameObject.SetActive(false);
        SetDeathPanel(false);
        SetRessHudVisibilty(false);
        optionPanel.SetActive(false);
        PlayerHudOverlay.SetActive(true);
        settingPanel.SetActive(false);
        damageOverlay.color = new Color(1, 1, 1, 0);
    }

    private void Update()
    {
        if(GameManager.instance.CurrentGameStatus.Equals(Gamestatus.running))
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                setOptionPanel();
            }
        }
    }

    private void setOptionPanel()
    {
        if(settingPanel.activeInHierarchy)
        {
            settingPanel.SetActive(false);
            PlayerHudOverlay.SetActive(true);
            GameManager.instance.HideCursor();
            GameManager.instance.IsOptionOpen = false;
        }
        else
        {
            if(optionPanel.activeInHierarchy)
            {
                optionPanel.SetActive(false);
                PlayerHudOverlay.SetActive(true);
                GameManager.instance.IsOptionOpen = false;
                GameManager.instance.HideCursor();
            }
            else
            {
                optionPanel.SetActive(true);
                PlayerHudOverlay.SetActive(false);
                GameManager.instance.IsOptionOpen = true;
                GameManager.instance.ShowCursor();
            }
        }
    }

    public void OpenSettings()
    {
        settingPanel.SetActive(true);
        optionPanel.SetActive(false);
    }

    public void OnBossTrigger()
    {
        bossHealthBar.gameObject.SetActive(true);
    }

    public void OnBossDamage(float healthRatio)
    {
        bossHealthBar.HealthBarUpdate(healthRatio);
    }

    public void CahngeArtifact(Sprite _sprite)
    {
        artifactImage.sprite = _sprite;
    }

    public void ManaBarUpdate(float _fill)
    {
        manaBar.fillAmount = _fill;
    }

    public void HpBarUpdate(float _fill)
    {
        hpBar.fillAmount = _fill;
        hpBar.color = Color.Lerp(lowHealthColor, fullHealthColor, _fill);
    }

    public void SetDeathPanel(bool visible)
    {
        DeathPanel.SetActive(visible);
    }

    public void SetRessHudVisibilty(bool visible)
    {
        ressHud.gameObject.SetActive(visible);
    }

    public void SetRessFill(float ammount)
    {
        ressHud.SetFillBar(ammount);
    }

    public void LeaveRoom()
    {
        NetworkManager.instance.LeaveRoom();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Quit()
    {
        NetworkManager.instance.Quit();
    }

    public void OnTakeDamage()
    {
        StopCoroutine(damageOverlayCoroutine());
        StartCoroutine(damageOverlayCoroutine());
    }

    IEnumerator damageOverlayCoroutine()
    {

        float currentAlpha = 1;

        while(currentAlpha > 0)
        {
            damageOverlay.color = new Color(1, 1, 1, currentAlpha);
            currentAlpha -= Time.deltaTime;
            yield return null;
        }

        yield return null;
    }
}
