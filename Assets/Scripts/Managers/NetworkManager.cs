﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public enum NetworkStatus
{
    offline,
    howToPlay,
    linking,
    connectedLobby,
    ConnectedRoom,
    dungeonStarting,
    lore,
}



public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager instance;

    [Header("Networking Param")]
    [Tooltip("indicate max player per Room")]
    [SerializeField] byte MaxPlayerPerLobby = 2;
    [SerializeField] int NetSendRate = 20;
    [SerializeField] int NetSerializationRate = 15;

    [SerializeField] float ChangeLevelDelay = 5;

    private string _networkMessage;
    public string NetworkMessage { get { return _networkMessage; } }

    private NetworkStatus CurrentNetworkStatus;
    public NetworkStatus _CurrentnetworkStatus { get { return CurrentNetworkStatus; } }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        PhotonNetwork.AutomaticallySyncScene = true;
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        PhotonNetwork.SendRate = NetSendRate;
        PhotonNetwork.SerializationRate = NetSendRate;

        Offline();
    }

    private void Offline()
    {
        NetMessageUpdate();
        ChangeNetStatus(NetworkStatus.offline);
    }

    public void Connect()
    {
        PhotonNetwork.OfflineMode = false;
        NetMessageUpdate();
        PhotonNetwork.ConnectUsingSettings();
        ChangeNetStatus(NetworkStatus.linking);
    }

    public override void OnConnectedToMaster()
    {
        NetMessageUpdate();
        if(PhotonNetwork.NetworkClientState != ClientState.JoiningLobby)
            PhotonNetwork.JoinLobby();
    }

    public override void OnJoinedLobby()
    {
        NetMessageUpdate();
        ChangeNetStatus(NetworkStatus.connectedLobby);
    }

    public override void OnLeftLobby()
    {
        Disconnect();
    }
    

    public void Disconnect()
    {
        PhotonNetwork.Disconnect();
        NetMessageUpdate();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Offline();
        NetMessageUpdate();
    }

    public void MatchMaking()
    {
        NetMessageUpdate();
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        NetMessageUpdate();
        int randomRoomID = Random.Range(1000, 9999);

        RoomOptions currentRoomOptions = new RoomOptions();
        currentRoomOptions.MaxPlayers = MaxPlayerPerLobby;
        PhotonNetwork.CreateRoom("Room#"+randomRoomID, currentRoomOptions);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        NetMessageUpdate();
        Disconnect();
    }

    public override void OnJoinedRoom()
    {
        ChangeNetStatus(NetworkStatus.ConnectedRoom);
        NetMessageUpdate();
        RefreshRoom();
    }

    public override void OnLeftRoom()
    {
        if(SceneManager.GetActiveScene().buildIndex != 0)
            SceneManager.LoadScene(0);

        NetMessageUpdate();
        RefreshRoom();
    }

    private void RefreshRoom()
    {
        int playerInRoom = PhotonNetwork.PlayerList.Length;
        HudPlayerPreview();
        NetMessageUpdate();

        if(PhotonNetwork.InRoom && playerInRoom >= PhotonNetwork.CurrentRoom.MaxPlayers)
        {
            if(PhotonNetwork.IsMasterClient)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;
                StartCoroutine(StartDungeon());
            }
            ChangeNetStatus(NetworkStatus.dungeonStarting);
        }
    }

    private void HudPlayerPreview()
    {
        if (NetworkGui.instance)
        {
            NetworkGui.instance.SetPreviewAvatar();
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        RefreshRoom();
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        RefreshRoom();
    }

    public void LeaveQueue()
    {
        NetMessageUpdate();

            PhotonNetwork.LeaveRoom();
        
    }

    public void ChangeNetStatus(NetworkStatus _nextStatus)
    {

        CurrentNetworkStatus = _nextStatus;
        if (NetworkGui.instance)
            NetworkGui.instance.NetworkPanelUpdate(CurrentNetworkStatus);

        if(CameraMoveSelectionScene.Instance)
        {
            CameraMoveSelectionScene.Instance.ChangeCameraPosition(_nextStatus);
        }

        if(PhotonNetwork.CountOfPlayers >= 20)
        {
            Disconnect();
        }

    }

    private void NetMessageUpdate()
    {
        _networkMessage = PhotonNetwork.NetworkClientState.ToString();
        if(NetworkGui.instance)
            NetworkGui.instance.NetTextUpdate(_networkMessage);
    }

    public void Quit()
    {
        Disconnect();
        Application.Quit();
    }

    public void OfflineConnect()
    {
        PhotonNetwork.OfflineMode = true;
        Debug.Log("connecting offline");
        SceneManager.LoadScene(1);
    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.OfflineMode)
        {
            SceneManager.LoadScene(0);
            Disconnect();
            Debug.Log("leaving in offline");
        }
        else
        {
            ChangeNetStatus(NetworkStatus.connectedLobby);
            PhotonNetwork.LeaveRoom();
        }
    }

    IEnumerator StartDungeon()
    {
        yield return new WaitForSeconds(ChangeLevelDelay);
        PhotonNetwork.LoadLevel(1);
        yield return null;
    }
}
