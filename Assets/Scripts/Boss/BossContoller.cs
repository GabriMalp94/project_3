﻿using Photon.Pun;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BossStatus
{
    sleep,
    fighting,
    immune,
    dead,
}

public enum MaskType
{
    none,
    fire,
    earth,
    wind,
    water,
}


public class BossContoller : MonoBehaviour
{
    public static BossContoller instance;

    private float _maxHealth;
    private float _currentHealth;

    [SerializeField] float rotSpeed;
    private float rotY;

    private BossStatus _currentBossStatus;
    public BossStatus CurrentBossStatus { get { return _currentBossStatus; } }

    [SerializeField] List<Mask> _masks;
    public List<Mask> Masks { get { return _masks; } }

    private int currentMaskIndex = 0;
    [SerializeField] float MaskChangeTime = 5;
    private float currentMaskTime = 0;
    private float reductCastTime = 0;

    private List<GameObject> _bossAdds = new List<GameObject>();
    public List<GameObject> BossAdds { get { return _bossAdds; } }

    [SerializeField] int healerAddCount = 2;
    private BossAdds[] healerAdds;
    public BossAdds[] HealerAdds { get { return healerAdds; } }

    PhotonView _photonView;
    private List<Ability> OnDeathAbilityList = new List<Ability>();

    private AudioSource bossAudioSource;


    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        rotY = transform.localEulerAngles.y;
        bossAudioSource =  GetComponent<AudioSource>();
        instance = this;
        healerAdds = new BossAdds[healerAddCount];
    }

    private void Start()
    {
        foreach(Mask m in _masks )
        {
            _maxHealth += m.MaxHealth;
            m.gameObject.SetActive(false);
        }
        _currentBossStatus = BossStatus.sleep;
        RefreshHp();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<PlayerController>(out PlayerController controller) && PhotonNetwork.IsMasterClient && _currentBossStatus.Equals(BossStatus.sleep))
        {
            Debug.LogError("boss trigger");
            TriggerBossRPC();
            _photonView.RPC("TriggerBossRPC", RpcTarget.Others);
        }
    }

    private void Update()
    {
        if(PhotonNetwork.IsMasterClient && GameManager.instance.CurrentGameStatus == Gamestatus.running)
        {

            switch(_currentBossStatus)
            {
                case BossStatus.fighting:
                    BossCycle();
                    rotate();
                    break;

                case BossStatus.immune:
                    BossCycle();
                    rotate();
                    bool killedAdds = true;

                    foreach(var add in _bossAdds)
                    {
                        if(add != null)
                        {
                            killedAdds = false;
                            break;
                        }
                    }

                    if(killedAdds)
                    {
                        _bossAdds.Clear();
                        _currentBossStatus = BossStatus.fighting;
                    }
                    break;
            }
        }
    }

    public void setImmunity()
    {
        if(_currentBossStatus.Equals(BossStatus.fighting))
        {
            _currentBossStatus = BossStatus.immune;       
        }
    }



    private void rotate()
    {
        rotY += rotSpeed * Time.deltaTime;
        Quaternion rotation = Quaternion.Euler(0, rotY, 0);
        transform.localRotation = rotation;
    }

    private void BossCycle()
    {

        if(currentMaskTime < (MaskChangeTime - MaskChangeTime*reductCastTime))
        {
            currentMaskTime += Time.deltaTime;
        }
        else
        {
            currentMaskTime = 0;
            for(int i = 0; i < _masks.Count; i++)
            {
                if(!_masks[currentMaskIndex].IsDead)
                {
                    _masks[currentMaskIndex].startCast();
                    indexIncrement();
                    break;

                }
                else
                {
                    indexIncrement();
                }
            }
        }
    }


    public void CastDeathMaskEffect()
    {
        foreach (var spell in OnDeathAbilityList)
        {
            spell.OnDeathSpellEffect();
        }
    }

    public void OnDeathMask(MaskType _type)
    {

        Ability currentAbility = AbilityReference.instance.GetMaskAbility(_type);
        if(currentAbility)
        {
            if(currentAbility.OnDeathCastIstant)
            {
                currentAbility.OnDeathSpellEffect();
            }
            else
            {
                OnDeathAbilityList.Add(currentAbility);
            }
        }
    }

    public void OnMaskDeath()
    {
        int maskAlive = 0;
        foreach (var m in Masks)
        {
            if (!m.IsDead)
            {
                maskAlive++;
            }
        }
        if (maskAlive == 1)
        {
            SoundManager.Instance.LastMaskMusic();
        }
    }

    private void indexIncrement()
    {
        currentMaskIndex++;

        if (currentMaskIndex >= _masks.Count)
        {
            currentMaskIndex = 0;
        }
    }

    [PunRPC]
    private void TriggerBossRPC()
    {
        foreach(var mask in _masks)
        {
            mask.gameObject.SetActive(true);
            mask.SetShader();
        }
        bossAudioSource.PlayOneShot(SoundManager.Instance.SFX_Boss_Scream);
        _currentBossStatus = BossStatus.fighting;
        HudManager.instance.OnBossTrigger();
        SoundManager.Instance.TriggerBackGroundMusic();
    }

    public void RefreshHp()
    {
        _currentHealth = 0;
        foreach(Mask m in _masks)
        {
            _currentHealth += m.CurrentHealth;
            HudManager.instance.OnBossDamage(_currentHealth / _maxHealth);            
        }
        Debug.LogError("total boos health: " + _currentHealth);
    }

    public void SetCastReduction(float _reduction)
    {
        reductCastTime = _reduction;
        Debug.LogError("boss cast time: " + (MaskChangeTime - MaskChangeTime * reductCastTime));
    }

    public void MaskDeath_SFX()
    {
        Debug.Log("play sound");
        bossAudioSource.PlayOneShot(SoundManager.Instance.SFX_Mask_Death);
    }
}
