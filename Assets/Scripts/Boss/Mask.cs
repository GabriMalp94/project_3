﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using System.Linq;

public class Mask : ShatterableMask
{

    private bool isActive;

    [Range(0, 1)]
    [SerializeField] private float castShaderSpeed;

	private Ability _bossAbility;

    [SerializeField]

	private void Start()
	{
        if(PhotonNetwork.IsMasterClient && maskType != MaskType.none)
        {
            _bossAbility = AbilityReference.instance.GetMaskAbility(maskType);
        }
	}

    public void startCast()
    {
        StartCastRPC();
        photonView.RPC("StartCastRPC", RpcTarget.Others);
    }

    [PunRPC]
    private void StartCastRPC()
    {
        StartCoroutine(castShading());
    }

    IEnumerator castShading()
    {

        float startEmissionAmmount = _meshrendererList[0].material.GetFloat("_emissionAmmount");
        float currentEmissionAmmount = startEmissionAmmount;

        while(currentEmissionAmmount < 1)
        {
            currentEmissionAmmount += Time.deltaTime * castShaderSpeed;
            
            foreach(var mr in _meshrendererList)
            {
                mr.material.SetFloat("_emissionAmmount", currentEmissionAmmount);
            }
            yield return null;
        }

        if(PhotonNetwork.IsMasterClient)
        {
		    Debug.LogError("casting spell: " + _bossAbility.SpellName);
		    _bossAbility.spellEffect();
            BossContoller.instance.CastDeathMaskEffect();
        }
		

        while(currentEmissionAmmount > startEmissionAmmount)
        {
            currentEmissionAmmount -= Time.deltaTime * castShaderSpeed;

            foreach (var mr in _meshrendererList)
            {
                mr.material.SetFloat("_emissionAmmount", currentEmissionAmmount);
            }
            yield return null;
        }

        yield return null;
    }

   

}
