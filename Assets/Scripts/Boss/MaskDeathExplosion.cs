﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaskDeathExplosion : MonoBehaviour
{
    [SerializeField] private LayerMask currentLayer;
    [SerializeField] private float checkGameOverDelay;

    private void Start()
    {
        GameManager.instance.VictoryCheck();
        RaycastHit hit;
        if(Physics.Linecast(BossContoller.instance.transform.position, GameManager.instance.LocalPlayer.transform.position, out hit, currentLayer))
        {
            Debug.Log(hit.collider.gameObject.name);
            if(hit.collider.TryGetComponent<PlayerResources>(out PlayerResources currentPlaeyr))
            {
                currentPlaeyr.TakeDamage(100000);
            }
        }
    }
}
