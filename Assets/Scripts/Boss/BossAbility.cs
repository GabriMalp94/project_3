﻿using UnityEngine;

[System.Serializable]
public class BossAbility
{
	[SerializeField] MaskType _currentMaskType;
	public MaskType CurrentMaskType { get { return _currentMaskType; } }

	[SerializeField] Ability _currentAbility;
	public Ability CurrentAbility { get { return _currentAbility; } }
}
