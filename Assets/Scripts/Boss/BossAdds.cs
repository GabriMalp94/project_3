﻿using Photon.Pun;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class BossAdds : ShatterableMask
{

    [SerializeField] private float healBossOnMaskDeathPerSecond = 5f;
    [SerializeField] EarthAddSpell earthAddSpell;

    [SerializeField] float deathDelay = 2;

    private void Start()
    {
        StartCoroutine((dissolve()));
        transform.LookAt(BossContoller.instance.transform);
    }

    public void StartHeal()
    {
        if(PhotonNetwork.IsMasterClient)
        {
            StartCoroutine(healBossCoroutine());
        }
    }

    protected override void onDeath()
    {
        Destroy(this.gameObject, deathDelay);
    }

    IEnumerator healBossCoroutine()
    {
        GameObject currentSpell = GameManager.instance.NetInstance(earthAddSpell.name, transform.position, Quaternion.identity);
        while(!isDead)
        {
            foreach(var m in BossContoller.instance.Masks)
            {
                if(m && !m.IsDead && m.CurrentHealth < m.MaxHealth)
                {
                    m.HealMask(healBossOnMaskDeathPerSecond);
                }
            }
            yield return new WaitForSeconds(1);
        }
        PhotonNetwork.Destroy(currentSpell);
        yield return null;
    }
}
