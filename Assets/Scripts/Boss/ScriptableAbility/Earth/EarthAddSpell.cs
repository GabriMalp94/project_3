﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthAddSpell : MonoBehaviour
{
    [SerializeField] Transform startPosition;
    [SerializeField] LineRenderer rayVFX;

    private void Start()
    {
        rayVFX.SetPosition(0, startPosition.position);
        rayVFX.SetPosition(1, BossContoller.instance.transform.position);
    }
}
