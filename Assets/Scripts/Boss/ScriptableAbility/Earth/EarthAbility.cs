﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/abilities/boss/earth"))]
public class EarthAbility : Ability
{
    
    [SerializeField] int addsCount = 5;
    [SerializeField] int maxOffset = 30;
    [SerializeField] int minOffset = 10;
    [SerializeField] float YOffset = -5;
    [SerializeField] GameObject addsPrefab;

	public override void spellEffect()
	{
        int addsToInstance = addsCount - BossContoller.instance.BossAdds.Count;

        Debug.LogError(addsToInstance);

        for(int i = 0; i < addsToInstance; i ++)
        {
            float randomAngle = Random.Range(0, 360);
            int randomOffset = Random.Range(minOffset, maxOffset);

            Vector3 totemPos = BossContoller.instance.transform.position + (Quaternion.Euler(0, randomAngle, 0) * new Vector3(0, YOffset, randomOffset));
            GameObject currentAdd = GameManager.instance.NetInstance(addsPrefab.name, totemPos, Quaternion.identity);
            BossContoller.instance.BossAdds.Add(currentAdd);
        }

        if(BossContoller.instance.CurrentBossStatus != BossStatus.immune)
        {
            GameManager.instance.NetInstance(SpellVFX.name, BossContoller.instance.transform.position, Quaternion.identity);
            BossContoller.instance.setImmunity();
        }
	}

    public override void OnDeathSpellEffect()
    {

        for (int i = 0; i < BossContoller.instance.HealerAdds.Length; i++)
        {
            if(BossContoller.instance.HealerAdds[i] == null || BossContoller.instance.HealerAdds[i].IsDead)
            {
                float randomAngle = Random.Range(0, 360);
                int randomOffset = Random.Range(minOffset, maxOffset);

                Vector3 totemPos = BossContoller.instance.transform.position + (Quaternion.Euler(0, randomAngle, 0) * new Vector3(0, 0, randomOffset));
                GameObject currentAdd = GameManager.instance.NetInstance(addsPrefab.name, totemPos, Quaternion.identity);
                currentAdd.GetComponent<BossAdds>().StartHeal();
                BossContoller.instance.HealerAdds[i] = currentAdd.GetComponent<BossAdds>(); ;
                Debug.LogError("earth post mortem spell");
                break;
            }
        }
    }
}
