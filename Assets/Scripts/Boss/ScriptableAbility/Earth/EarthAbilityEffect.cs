﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class EarthAbilityEffect : AbilityEffect
{
    [SerializeField] float explosiveRadius = 25;
    [SerializeField] float explosiveForce = 20;

    [SerializeField] ParticleSystemRenderer shield_hit_VFX;

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
    }

    protected override IEnumerator AbilityCoroutine()
    {
        if(PhotonNetwork.IsMasterClient)
        {

            while (true)
            {
                if (!BossContoller.instance.CurrentBossStatus.Equals(BossStatus.immune))
                {
                    Debug.LogError("immunity Break");
                    PhotonNetwork.Destroy(GetComponent<PhotonView>());
                    break;
                }

                yield return null;
            }
        }

        yield return null;
    }


    private void OnCollisionEnter(Collision collision)
    {
        OnHitCheck(collision.gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        OnHitCheck(other.gameObject);
    }

    private void OnHitCheck(GameObject collision)
    {
        if (collision.TryGetComponent<ShieldHitter>(out ShieldHitter hit))
        {
            Vector3 position = hit.transform.position;

            OnShieldHit(position.x, position.y, position.z);
            _photonView.RPC("OnShieldHit", RpcTarget.Others, position.x, position.y, position.z);
            
        }
    }

    [PunRPC]
    private void OnShieldHit(float x, float y, float z)
    {
        Vector3 position = new Vector3(x, y, z);
        ParticleSystemRenderer currentHit = GameManager.instance.NetInstance(shield_hit_VFX.name, transform.position, Quaternion.identity).GetComponent<ParticleSystemRenderer>();
        currentHit.material.SetVector("_center", position);
    }
}
