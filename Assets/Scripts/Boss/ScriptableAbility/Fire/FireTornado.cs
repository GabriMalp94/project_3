﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.Linq;

public class FireTornado : MonoBehaviour
{
    [SerializeField] private float speed = 15f;
    [SerializeField] private float changeDirectionDetectRange = 3f;
    [SerializeField] private float damagePerSecond = 20f;
    [SerializeField] private float radius = 5;

    private List<PlayerResources> playerList = new List<PlayerResources>();

    private PhotonView photonView;

    float rotaY;

    private void Awake()
    {
        photonView = GetComponent<PhotonView>();
    }

    private void Start()
    {
        rotaY = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.CurrentGameStatus != Gamestatus.running)
        {
            Destroy(this.gameObject);
        }
        if(photonView.IsMine)
        {
            move();
        }

        foreach(var player in playerList)
        {
            if(player.CurrentHealth > 0)
            {
                player.TakeDamage(damagePerSecond * Time.deltaTime);
                Debug.Log(player.name + " take damage");
            }
            else
            {
                playerList.Remove(player);
            }
        }
    }

    private void move()
    {
        transform.position += transform.forward * speed * Time.deltaTime;

        RaycastHit hit;
        if (Physics.Raycast(transform.position + transform.up * 5, transform.forward, out hit, changeDirectionDetectRange))
        {
            if (hit.collider.gameObject.tag != "Player" && hit.collider.gameObject.tag != "Boss")
            {
                rotaY = Random.Range(0, 360);
                Quaternion rotation = Quaternion.Euler(0, rotaY, 0);
                transform.rotation = rotation;
            }
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(transform.position + transform.up * 5, (transform.forward * changeDirectionDetectRange));
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<PlayerResources>(out PlayerResources currentPlayer))
        {
            Debug.Log(other.name + " enter tornado");
            playerList.Add(currentPlayer);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerResources>(out PlayerResources currentPlayer))
        {
            Debug.Log(other.name + " exit tornado");
            playerList.Remove(currentPlayer);
        }
    }
}
