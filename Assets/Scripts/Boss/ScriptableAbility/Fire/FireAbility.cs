﻿using UnityEngine;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName =("custom/abilities/boss/fire"))]
public class FireAbility : Ability
{
	[SerializeField] private int colomnAmmount;
    [SerializeField] private float maxDistanceFromPlayer;

    [SerializeField] private GameObject OnDeathMaskVFX;


    public override void spellEffect()
    {
        List<GameObject> instantiatedSpell = new List<GameObject>();

        for(int i = 0; i < colomnAmmount; i++)
        {
            float offset = Random.Range(-maxDistanceFromPlayer, maxDistanceFromPlayer);
            Vector3 distance = new Vector3(0, 0, offset);

            float randomAngle = Random.Range(0, 360);
            Quaternion rotation = Quaternion.Euler(0, randomAngle, 0);

            int randomPlayerIndex = Random.Range(0, GameManager.instance.PlayerList.Count);

            Vector3 coloumnPosition = GameManager.instance.PlayerList[randomPlayerIndex].transform.position + (rotation * distance);

			GameManager.instance.NetInstance(SpellVFX.name, coloumnPosition, Quaternion.identity);
        }
	}

    public override void OnDeathSpellEffect()
    {
        GameManager.instance.NetInstance(OnDeathMaskVFX.name, Vector3.zero, Quaternion.identity);
        Debug.LogError("fire post mortem spell");

    }
}
