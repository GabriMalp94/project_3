﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAbilityEffect : AbilityEffect
{
    [SerializeField] private float damage;
    [SerializeField] private float damageDelay;
    [SerializeField] private float damageRadius;
    [SerializeField] private AudioSource audioSource;

    protected override IEnumerator AbilityCoroutine()
    {
        yield return new WaitForSeconds(damageDelay);
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();

        Collider[] hitsObject = Physics.OverlapSphere(transform.position, damageRadius);
        foreach (Collider c in hitsObject)
        {
            if (c.gameObject.TryGetComponent<PlayerResources>(out PlayerResources _currentPlayer))
            {
                _currentPlayer.TakeDamage(damage);
            }
        }

        yield return null;
    }
}
