﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class WaterBall : MonoBehaviour
{
    private Rigidbody rigidBody;
    private PhotonView photonView;

    [SerializeField] float debugForce;

    [SerializeField] GameObject WaterPoolVFX;

    bool casted = false;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        photonView = GetComponent<PhotonView>();
    }

    public void Shoot(float _force)
    {
        ShootRPC(_force);
        photonView.RPC("ShootRPC", RpcTarget.Others, _force);
    }

    [PunRPC]
    private void ShootRPC(float _force)
    {
        rigidBody.AddForce(transform.forward * _force, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!casted && other.tag == "Ground")
        {
            Instantiate(WaterPoolVFX, transform.position, Quaternion.identity);
            casted = true;
            Destroy(this.gameObject);
        }
    }
}
