﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterCross : MonoBehaviour
{
    [SerializeField] float damage = 20;
    [SerializeField] float destroyTime = 3;


    float currentTime = 0;

    Vector3 startSize;
    [SerializeField] Vector3 finalSize;

    [SerializeField] float scaleSpeed = 1;

    List<PlayerResources> hittedPlayer = new List<PlayerResources>();
    // Start is called before the first frame update
    void Start()
    {
        startSize = transform.localScale;
        Destroy(this.gameObject, destroyTime);
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        transform.localScale = Vector3.Lerp(startSize, finalSize, currentTime * scaleSpeed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<PlayerResources>(out PlayerResources currentPlayer))
        {
            bool canDamage = true;
            foreach(var player in hittedPlayer)
            {
                if (player == currentPlayer)
                {
                    canDamage = false;
                    break;
                }
            }
            if(canDamage)
            {
                currentPlayer.TakeDamage(damage);
                hittedPlayer.Add(currentPlayer);
            }
        }
    }
}
