﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterAbilityEffect : AbilityEffect
{
    [SerializeField] float damage = 20;
    [SerializeField] float damageDelay = 10;
    private void Awake()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent<PlayerController>(out PlayerController currentPlayer) && currentPlayer.PlayerPhotonView.IsMine)
        {
            Debug.LogError("player Entered");
            currentPlayer.waterImmune = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerController>(out PlayerController currentPlayer) && currentPlayer.PlayerPhotonView.IsMine)
        {
            Debug.LogError("player Exit");
            currentPlayer.waterImmune = false;
        }
    }

    protected override IEnumerator AbilityCoroutine()
    {
        float timer = damageDelay;
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }       

        if(!GameManager.instance.LocalPlayer.waterImmune)
        {
            PlayerResources currentResource = GameManager.instance.LocalPlayer.GetComponent<PlayerResources>();
            currentResource.TakeDamage(damage);
            Debug.LogError("not immune water");
        }
        else
        {
            Debug.LogError("Water immunity");
        }

        yield return new WaitForSeconds(1f);

        GameManager.instance.LocalPlayer.waterImmune = false;

        Destroy(this.gameObject);

        yield return null;
    }
}
