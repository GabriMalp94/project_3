﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/abilities/boss/water"))]
public class WaterAbility : Ability
{
    [SerializeField] float GroundDistance;
    [SerializeField] GameObject waterball_VFX;
    [SerializeField] float waterBallShootForce;

    [SerializeField] float SphereXRotation;

    public override void spellEffect()
    {
        int randomIndex = Random.Range(0, GameManager.instance.PlayerList.Count);

        Vector3 destination = GameManager.instance.PlayerList[randomIndex].transform.position;
        Vector3 spellPosition = BossContoller.instance.transform.position + Vector3.up * GroundDistance;

        Vector3 distance = destination - spellPosition;

       WaterTier currentTier =  GameManager.instance.NetInstance(SpellVFX.name, spellPosition, Quaternion.LookRotation(distance)).GetComponent<WaterTier>();
    }

    public override void OnDeathSpellEffect()
    {
        float randomY = Random.Range(0, 360);
        Quaternion rotation = Quaternion.Euler(SphereXRotation, randomY, 0);
        WaterBall currentBall = GameManager.instance.NetInstance(waterball_VFX.name, BossContoller.instance.transform.position, rotation).GetComponent<WaterBall>();
        currentBall.Shoot(waterBallShootForce);
        Debug.LogError("water post mortem spell");
    }
}
