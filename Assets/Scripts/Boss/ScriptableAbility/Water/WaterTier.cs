﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTier : MonoBehaviour
{
    [SerializeField] GameObject cross_VFX;
    [SerializeField] float speed = 15;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Ground")
        {
            Instantiate(cross_VFX, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
        }
    }

    private void Update()
    {
        transform.position += transform.forward * speed * Time.deltaTime;    
    }
}
