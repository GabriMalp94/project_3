﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class WindEffect : AbilityEffect
{
    [SerializeField] private float damagePerSecond = 20f;
    [SerializeField] private float damageDelay = 0.3f;
    [SerializeField] private float destructTime = 5;
    BoxCollider capsuleCollider;
    private List<PlayerResources> playerList = new List<PlayerResources>();

    private void Awake()
    {
        capsuleCollider = GetComponent<BoxCollider>();
        capsuleCollider.enabled = false;
        Destroy(this.gameObject, destructTime);
    }

    void Update()
    {
        foreach (var player in playerList)
        {
            if (player.CurrentHealth > 0)
            {
                player.TakeDamage(damagePerSecond * Time.deltaTime);
                Debug.Log(player.name + " take damage");
            }
            else
            {
                playerList.Remove(player);
            }
        }
    }

    protected override IEnumerator AbilityCoroutine()
    {
        yield return new WaitForSeconds(damageDelay);

        capsuleCollider.enabled = true;

        yield return null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent<PlayerResources>(out PlayerResources currentPlayer))
        {
            Debug.Log(other.name + " enter tornado");
            playerList.Add(currentPlayer);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent<PlayerResources>(out PlayerResources currentPlayer))
        {
            Debug.Log(other.name + " exit tornado");
            playerList.Remove(currentPlayer);
        }
    }

    public void SetWinMask(PhotonView mask)
    {
        _photonView = GetComponent<PhotonView>();
        transform.SetParent(mask.transform);
        _photonView.RPC("SetWindMaskRpc", RpcTarget.Others, mask.ViewID);
    }

    [PunRPC]
    public void SetWindMaskRpc(int id)
    {
        Transform mask = PhotonNetwork.GetPhotonView(id).transform;
        transform.SetParent(mask.transform);
    }
}
