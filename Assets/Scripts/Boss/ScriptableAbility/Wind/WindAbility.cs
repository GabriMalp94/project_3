﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/abilities/boss/wind"))]
public class WindAbility : Ability
{
    [Range(0, 1)]
    [SerializeField] float BossCastReduction = 0.4f;

    public override void spellEffect()
    {
        foreach(var m in BossContoller.instance.Masks)
        {
            if(m != null)
            {
                GameObject spell = GameManager.instance.NetInstance(SpellVFX.name, m.transform.position, m.transform.rotation);
                spell.GetComponent<WindEffect>().SetWinMask(m.MaskPhotonView);
            }
        }
    }

    public override void OnDeathSpellEffect()
    {
        Debug.LogError("wind post mortem spell");
        BossContoller.instance.SetCastReduction(BossCastReduction);
    }
}
