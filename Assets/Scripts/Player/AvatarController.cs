﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarController : MonoBehaviour
{
    PlayerAnim _playeranim;
    public PlayerAnim AvatarAnimator { get { return _playeranim; } }

    [SerializeField] Camera _camera;
    public Camera PlayerCamera { get { return _camera; } }

    [SerializeField] Transform _spellPosition;
    public Transform SpellSPosition { get { return _spellPosition; } }

    [SerializeField] Transform CameraSocket;
    private PhotonView _photonView;

    private void Awake()
    {
        _playeranim = GetComponent<PlayerAnim>();
        _photonView = GetComponent<PhotonView>();

        if (_photonView.IsMine)
        {
            _camera.gameObject.SetActive(true);
        }
        else
        {
            Destroy(_camera.gameObject);

        }
    }

    private void Update()
    {
        if (_photonView.IsMine)
        {
            PlayerCamera.transform.position = CameraSocket.position;
        }
    }
}
