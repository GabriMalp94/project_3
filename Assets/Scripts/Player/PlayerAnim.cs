﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAnim : MonoBehaviour
{
    [SerializeField] Animator _animator;

    public void SetMoveAnimation(float vertical, float horizontal)
    {
        _animator.SetFloat("Vertical", vertical);
        _animator.SetFloat("Horizontal", horizontal);
    }

    public void SetDeathAnimation(bool isDead)
    {
        _animator.SetBool("IsDead", isDead);
    }

    public void CastAnimation(int _animID)
    {

        _animator.SetInteger("castAnimationNum", _animID);
        StartCoroutine(SmoothAnimation(0, 1));
    }

    IEnumerator SmoothAnimation(float start, float end)
    {
        float currentTime = 0;

        while(currentTime < 1)
        {
            float currentValue = Mathf.Lerp(start, end, currentTime);
            _animator.SetLayerWeight(1, currentValue);
            currentTime += Time.deltaTime;
            yield return null;
        }

        _animator.SetInteger("castAnimationNum", 0);


        while (currentTime > 0)
        {
            float currentValue = Mathf.Lerp(start, end, currentTime);
            _animator.SetLayerWeight(1, currentValue);
            currentTime -= Time.deltaTime;
            yield return null;
        }

        Debug.Log("end animation");

        GameManager.instance.LocalPlayer.LocalPlayerAbility.CastDelayed = true;
        yield return null;
    }
}
