﻿using Photon.Pun;
using UnityEngine;

public enum PlayerStatus
{
    Alive,
    dead,
}



public class PlayerController : MonoBehaviour
{

    Rigidbody _rigidbody;
    [Header("Movement Param")]
    [SerializeField] float forwardSpeed = 10f;
    private float _speedBonus = 0;
    public float SpeedBonus { get { return _speedBonus; } set { _speedBonus = value; } }
    

    [Tooltip("speed cut down when walking back 0% - 100%")]
    [Range(0, 1)] [SerializeField] float WalkBackSpeedCutDown;
    [SerializeField] float PlayerRotSpeed = 10f;
    [Header("Camera param")]

    [SerializeField] float maxVerticalAngle = 60;
    [SerializeField] float minVerticalAngle = - 60;
    [SerializeField] float CameraSensit;

    private int _playerID;
    public int PlayerID { get { return _playerID; } }

    private float CameraRotX;
    private float PlayerRotY;

    private PhotonView _photonView;
    public  PhotonView PlayerPhotonView {  get { return _photonView; } }

    private AvatarController _avatarController;
    public AvatarController PlayerAvatarController { get { return _avatarController; } }

	private PlayerResources _playerResources;
	public PlayerResources playerRescources { get { return _playerResources; } }

    private PlayerAbility localPlayerAbility;
    public PlayerAbility LocalPlayerAbility { get { return localPlayerAbility; } }

    private PlayerStatus _currentPlayerStatus;
    public PlayerStatus CurrentPlayerStatus { get { return _currentPlayerStatus; } set { _currentPlayerStatus = value; } }

    public bool waterImmune = false;
    public bool moveImparing = false;

    private AudioSource playerAudioSource;
    public AudioSource PlayerAudioSource { get { return playerAudioSource; } }

    private float currentTime = 0;

    public void SetPlayerID(int _playerID)
    {
        SetPlayerIDRPC(_playerID);
        _photonView.RPC("SetPlayerIDRPC", RpcTarget.Others, _playerID);

        _avatarController = PhotonNetwork.Instantiate(PrefabManager.instance.AvatarsPrefab[_playerID].name, transform.position, transform.rotation).GetComponent<AvatarController>();
        _avatarController.transform.SetParent(this.transform);

        _photonView.RPC("SetAvatar", RpcTarget.Others, _avatarController.GetComponent<PhotonView>().ViewID);

        
    }

    [PunRPC]
    private void SetPlayerIDRPC(int _ID)
    {
        _playerID = _ID;
    }

    [PunRPC]
    private void SetAvatar(int viewID)
    {
        var avatar = PhotonNetwork.GetPhotonView(viewID);
        avatar.transform.SetParent(this.transform);
        avatar.transform.localPosition = Vector3.zero;
        avatar.transform.localRotation = Quaternion.identity;

    }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _photonView = GetComponent<PhotonView>();
		_playerResources = GetComponent<PlayerResources>();
        localPlayerAbility = GetComponent<PlayerAbility>();
        playerAudioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        if(_photonView.IsMine)
        {
            PlayerRotY = transform.eulerAngles.y;
            CameraRotX = _avatarController.PlayerCamera.transform.localEulerAngles.x;
            GameManager.instance.SpawnedPlayer(_photonView.ViewID);
            _currentPlayerStatus = PlayerStatus.Alive;
            moveImparing = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       if(_photonView.IsMine && !GameManager.instance.IsOptionOpen)
        {
            switch(_currentPlayerStatus)
            {
                case PlayerStatus.Alive:
                    MoveCamera();
                    PlayerRotate();
                    PlayerMove();
                    break;

                case PlayerStatus.dead:
                    break;
            }
        }
    }

    private void MoveCamera()
    {
        
        CameraRotX -= Input.GetAxis("Mouse Y") * SettingsManager.Instance.cameraMouseSensit*Time.fixedDeltaTime;
        CameraRotX = Mathf.Clamp(CameraRotX, minVerticalAngle, maxVerticalAngle);

        Quaternion rotation = Quaternion.Euler(CameraRotX, 0, 0);
        _avatarController.PlayerCamera.transform.localRotation = rotation;
    }

    private void PlayerRotate()
    {
        PlayerRotY += Input.GetAxis("Mouse X") * SettingsManager.Instance.cameraMouseSensit * Time.fixedDeltaTime;
        Quaternion rotation = Quaternion.Euler(0, PlayerRotY, 0);
        transform.rotation = rotation;
    }

    private void PlayerMove()
    {
        if(!moveImparing)
        {
            float DeltaX = Input.GetAxis("Horizontal");
            float DeltaZ = Input.GetAxis("Vertical");

            _avatarController.AvatarAnimator.SetMoveAnimation(DeltaZ, DeltaX);

            float currentSpeed = DeltaZ >= 0 ? forwardSpeed + _speedBonus : (forwardSpeed+ _speedBonus) * WalkBackSpeedCutDown;

            Vector3 move = new Vector3(DeltaX, 0, DeltaZ);
            move *= currentSpeed;

            move = Vector3.ClampMagnitude(move, currentSpeed);
            move = transform.TransformDirection(move);

            if(move != Vector3.zero)
            {
                if(currentTime < 2 / (forwardSpeed + _speedBonus))
                {
                    currentTime += Time.deltaTime;
                }
                else
                {
                    currentTime = 0;
                    SetFoot_SFX();
                }
            }
            else
            {
                if(currentTime > 0)
                {
                    currentTime = 0;
                }
            }

            _rigidbody.MovePosition(transform.position + move * Time.fixedDeltaTime);
        }
        else
        {
            _avatarController.AvatarAnimator.SetMoveAnimation(0, 0);
        }
    }

    public void SetFoot_SFX()
    {
        RpcSetFoot();
        _photonView.RPC("RpcSetFoot", RpcTarget.Others);
    }

    [PunRPC]
    public void RpcSetFoot()
    {
        playerAudioSource.PlayOneShot(SoundManager.Instance.SFX_Player_Step);
    }
}
