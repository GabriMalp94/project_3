﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;


public class PlayerResources : MonoBehaviourPun
{

    [SerializeField] GameObject ressVFX;
    GameObject currentVFX;
	[SerializeField] private float _maxHealth;
	public float maxHealth { get { return _maxHealth; } }

	private float _currentHealth;
	public float CurrentHealth
    {
        get
        {
            return _currentHealth;
        }

        set
        {
            _currentHealth = value;
            HealthRatioRefresh();
            if(CurrentHealth <= 0)
            {
                _currentHealth = 0;
            }
            else if(CurrentHealth >= _maxHealth)
            {
                _currentHealth = _maxHealth;
            }
        }
    }

    [SerializeField] private float _maxMana = 100;
    public float MaxMana { get { return _maxMana; } }

    private float _currentMana;
    public float CurrentMana
    {
        get
        {
            return _currentMana;
        }
        set
        {
            _currentMana = value;
            ManaRatioRefresh();
            if (_currentMana < 0)
            {
                _currentMana = 0;
            }
            else if(_currentMana > _maxMana)
            {
                _currentMana = _maxMana;
            }
        }
    }

    private float damageReduction = 0;

    [Range(0,1)] private float ressHp = 0.5f;
    [SerializeField] float AllyRessTime;

    [SerializeField] private BoxCollider ressCollider;

    PlayerController playerController;

	private void Awake()
	{
		_currentHealth = _maxHealth;
        _currentMana = _maxMana;
		playerController = GetComponent<PlayerController>();
        ressCollider.enabled = false;
        
	}

    private void Start()
    {
        HealthRatioRefresh();
        ManaRatioRefresh();
    }
    public void TakeDamage(float _damage)
	{
		if(playerController.PlayerPhotonView.IsMine && playerController.CurrentPlayerStatus == PlayerStatus.Alive)
		{
            HudManager.instance.OnTakeDamage();
            float currentDamage = _damage * (1 - damageReduction);
            float currentManaConversion = _damage * damageReduction;


            if(currentManaConversion > _currentMana)
            {
                float returnedDamage = currentManaConversion - _currentMana;
                currentManaConversion = _currentMana;

                if(returnedDamage <= 0)
                {
                    Debug.LogError("conversion damage issue");
                }
                currentDamage += returnedDamage;
            }

			Debug.LogError(playerController.PlayerID +" damage: " + currentDamage + " assorbed: " + currentManaConversion);

            CurrentHealth -= currentDamage;
            CurrentMana -= currentManaConversion;
			if(_currentHealth <= 0)
			{
				_currentHealth = 0;
                playerController.PlayerAvatarController.AvatarAnimator.SetMoveAnimation(0, 0);
                playerController.PlayerAvatarController.AvatarAnimator.SetDeathAnimation(true);

                playerDead();
                playerController.PlayerPhotonView.RPC("playerDead", RpcTarget.Others);
			}
		}
	}

    private void HealthRatioRefresh()
    {
        float ratio = _currentHealth / maxHealth;

        HudManager.instance.HpBarUpdate(ratio);
    }

    private void ManaRatioRefresh()
    {
        float ratio = _currentMana / _maxMana;

        HudManager.instance.ManaBarUpdate(ratio);
    }

	[PunRPC]
	private void playerDead()
	{
		Debug.LogError("Player dead: " + playerController.PlayerID);
        ressCollider.enabled = true;
        playerController.CurrentPlayerStatus = PlayerStatus.dead;
        currentVFX = Instantiate(ressVFX, transform.position, Quaternion.identity);
        if(playerController.PlayerPhotonView.IsMine)
        {
            HudManager.instance.SetDeathPanel(true);
        }
        GameManager.instance.OnPlayerDeath();
	}

    public void Ress()
    {
        RessRPC();
        playerController.PlayerPhotonView.RPC("RessRPC", RpcTarget.Others);
        Debug.LogError("got ress");
    }

    [PunRPC]
    private void RessRPC()
    {
        _currentHealth = _maxHealth * ressHp;
        ressCollider.enabled = false;
        playerController.CurrentPlayerStatus = PlayerStatus.Alive;
        if(currentVFX)
        {
            Destroy(currentVFX);
        }
        if (playerController.PlayerPhotonView.IsMine)
        {
            playerController.PlayerAvatarController.AvatarAnimator.SetDeathAnimation(false);
            HudManager.instance.SetDeathPanel(false);
            HealthRatioRefresh();
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if(playerController.PlayerPhotonView.IsMine && playerController.CurrentPlayerStatus == PlayerStatus.Alive)
        {
            if(other.TryGetComponent<PlayerController>(out PlayerController currentPlayer) && currentPlayer.CurrentPlayerStatus.Equals(PlayerStatus.dead))
            {
                StartCoroutine(RessCoroutine(currentPlayer));
            }
        }
    }

    public void setShield(float reduction)
    {
        damageReduction = reduction;
    }

    private void OnTriggerExit(Collider other)
    {
        if (playerController.PlayerPhotonView.IsMine)
        {
            if (other.TryGetComponent<PlayerController>(out PlayerController currentPlayer))
            {
                HudManager.instance.SetRessHudVisibilty(false);
                StopCoroutine(RessCoroutine(currentPlayer));
            }
        }
    }

    IEnumerator RessCoroutine(PlayerController player)
    {
        float currentTime = 0;
        HudManager.instance.SetRessHudVisibilty(true);
        while(true)
        {
            Debug.LogError("Press F to ress");

            if(Input.GetKey(KeyCode.F))
            {
                currentTime += Time.deltaTime;
                if(currentTime >= AllyRessTime)
                {
                    player.playerRescources.Ress();
                    break;
                }
            }
            else
            {
                currentTime = 0;
            }

            HudManager.instance.SetRessFill(currentTime / AllyRessTime);
            yield return null;
        }

        HudManager.instance.SetRessHudVisibilty(false);

        yield return null;
    }
}
