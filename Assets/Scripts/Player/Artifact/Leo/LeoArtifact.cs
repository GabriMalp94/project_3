﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/artifact/leo"))]
public class LeoArtifact : Artifact
{
    public override IEnumerator ArtifactEffect(GameObject caster)
    {
        Debug.LogError("casting leo");
        if(caster.TryGetComponent<PlayerAbility>(out PlayerAbility currentPlayer) && caster.TryGetComponent<PlayerResources>(out PlayerResources playerResource) && playerResource.CurrentMana >= ManaCost)
        {
            currentPlayer.setSpecialAttack();
        }
        
        yield return null;
    }
}
