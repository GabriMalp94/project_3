﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/artifact/monkey"))]

public class MonkeyArtifact : Artifact
{
    [SerializeField] private float healthRegPerSecond = 10;
    [SerializeField] private float manaRegPerSecond = 10;
    public override IEnumerator ArtifactEffect(GameObject caster)
    {
        if (caster.TryGetComponent<PlayerResources>(out PlayerResources playerResource) && caster.TryGetComponent<PlayerController>(out PlayerController playerController))
        {
            Debug.LogError("casting monkey");
            GameObject currentVFX = GameManager.instance.NetInstance(abilityVFX.name, caster.transform.position, Quaternion.identity);

            playerController.moveImparing = true;
            playerController.gameObject.SendMessage("SetCanCast", false);

            while(playerController.CurrentPlayerStatus == PlayerStatus.Alive)
            {
                if (Input.GetKeyUp(KeyCode.Mouse1) || GameManager.instance.LocalPlayer.LocalPlayerAbility.EquipedArtifact != this)
                {
                    break;
                }
                playerResource.CurrentHealth += healthRegPerSecond * Time.deltaTime;
                playerResource.CurrentMana += manaRegPerSecond * Time.deltaTime;

                yield return null;
            }
            playerController.moveImparing = false;
            playerController.gameObject.SendMessage("SetCanCast", true);
            Photon.Pun.PhotonNetwork.Destroy(currentVFX.GetComponent<Photon.Pun.PhotonView>());
        }
        
        yield return null;
    }
    
}
