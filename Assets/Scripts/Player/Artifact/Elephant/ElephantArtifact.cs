﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

[CreateAssetMenu(menuName = ("custom/artifact/elephant"))]

public class ElephantArtifact : Artifact
{
    [Range(0,1)]
    [SerializeField] float damageConvertedToMana = 0.8f;
    public override IEnumerator ArtifactEffect(GameObject caster)
    {
        if (caster.TryGetComponent<PlayerResources>(out PlayerResources playerResource) && caster.TryGetComponent<PlayerController>(out PlayerController playerController))
        {
            Debug.LogError("casting elephant");
            if(playerResource.CurrentMana >= 0)
            {
                GameObject shield = GameManager.instance.NetInstance(abilityVFX.name, caster.transform.position, Quaternion.identity);
                playerResource.setShield(damageConvertedToMana);
                while (playerController.CurrentPlayerStatus == PlayerStatus.Alive)
                {
                    if (Input.GetKeyUp(KeyCode.Mouse1) || GameManager.instance.LocalPlayer.LocalPlayerAbility.EquipedArtifact != this || playerResource.CurrentMana <= 0)
                    {
                        break;
                    }

                    shield.transform.position = caster.transform.position;
                    yield return null;

                }

                PhotonNetwork.Destroy(shield.GetComponent<PhotonView>());
            }
        }
        playerResource.setShield(0);
        yield return null;
    }


}
