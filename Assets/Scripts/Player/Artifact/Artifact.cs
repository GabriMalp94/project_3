﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Artifact : ScriptableObject
{


    [SerializeField] private string _artifactName;
    public string ArtifactName { get { return _artifactName; } }

    [SerializeField] private Sprite _artifactSprite;
    public Sprite ArtifactSprite { get { return _artifactSprite; } }

    [SerializeField] protected GameObject abilityVFX;

    [SerializeField] private float _manaCost;
    public float ManaCost { get { return _manaCost; } }

    
    public virtual IEnumerator ArtifactEffect(GameObject caster)
    {
       

        yield return null;
    }


    protected bool manaCheck(GameObject caster)
    {
        if (caster.TryGetComponent<PlayerResources>(out PlayerResources currentPlayer))
        {
            if (currentPlayer.CurrentMana >= _manaCost)
            {
                currentPlayer.CurrentMana -= _manaCost;
                return true;
            }
            else
            {
                Debug.LogError("out of mana" + currentPlayer.CurrentMana);
                return false;
            }
        }

        return false;
    }
}
