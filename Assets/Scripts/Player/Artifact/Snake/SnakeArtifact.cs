﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[CreateAssetMenu(menuName = ("custom/artifact/snake"))]

public class SnakeArtifact : Artifact
{
    [SerializeField] float bonusSpeed = 15;

    public override IEnumerator ArtifactEffect(GameObject caster)
    {
        if(caster.TryGetComponent<PlayerResources>(out PlayerResources playerResource) && caster.TryGetComponent<PlayerController>(out PlayerController playerController))
        {
            Debug.LogError("casting snake");
            GameObject snake_VFX = GameManager.instance.NetInstance(abilityVFX.name, caster.transform.position, Quaternion.identity);
            playerController.SpeedBonus = bonusSpeed;
            while(playerController.CurrentPlayerStatus == PlayerStatus.Alive)
            {
                snake_VFX.transform.position = caster.transform.position;
                if(Input.GetKeyUp(KeyCode.Mouse1) || GameManager.instance.LocalPlayer.LocalPlayerAbility.EquipedArtifact != this || playerResource.CurrentMana <= 0)
                {
                    break;
                }

                playerResource.CurrentMana -= ManaCost * Time.deltaTime;
                yield return null;
            }

            playerController.SpeedBonus = 0;
            PhotonNetwork.Destroy(snake_VFX);
        }
            
        
        yield return null;
    }
}
