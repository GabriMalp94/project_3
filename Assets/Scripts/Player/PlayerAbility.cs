﻿using Photon.Pun;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerAbility : MonoBehaviour
{
    [SerializeField] Projectile[] SpellVFX;
    bool isSpecial = false;
    [SerializeField] float specialManaConsume = 50;

    bool canCast = true;

    [SerializeField] float castTime = 0.2f;
    private bool castDelayed = true;
    public bool CastDelayed { set { castDelayed = value; } }

    [SerializeField] List<Artifact> artifactList;
    private int equipeedArtifactIndex = 0;

    private Artifact equipedArtifact;
    public Artifact EquipedArtifact { get { return equipedArtifact; } }

    PlayerController playerController;

    private void Awake()
    {
        playerController = GetComponent<PlayerController>();

    }

    private void Start()
    {
        if (playerController.PlayerPhotonView.IsMine)
        {
            ChangeArtifact(0);
            canCast = true;
            castDelayed = true;
        }
    }
    private void Update()
    {
        if(playerController.PlayerPhotonView.IsMine && playerController.CurrentPlayerStatus == PlayerStatus.Alive && !GameManager.instance.IsOptionOpen)
        {


            if(Input.GetKeyDown(KeyCode.Mouse0))
            {
                if(canCast && castDelayed)
                {
                    CastSpell_SFX();
                    StartCoroutine(OffensiveSpell());
                    castDelayed = false;
                }
            }
            else if(Input.GetKeyDown(KeyCode.Mouse1))
            {
                ArtifactCast();
            }
            else if(Input.mouseScrollDelta.y != 0)
            {
                isSpecial = false;
                ChangeArtifact(Mathf.RoundToInt(Input.mouseScrollDelta.y));
            }
            else
            {
                ChangeArtifact();
            }

        }
    }

    private IEnumerator OffensiveSpell()
    {
        float currentManaSpend = isSpecial ? specialManaConsume : 0;
        CastCheck(ref currentManaSpend);
        string spellToCast = isSpecial ? SpellVFX[1].name : SpellVFX[0].name;
        playerController.playerRescources.CurrentMana -= currentManaSpend;

        int animationIndex = isSpecial ? 2 : 1;
        playerController.PlayerAvatarController.AvatarAnimator.CastAnimation(animationIndex);

        yield return new WaitForSeconds(castTime);
        Debug.Log("Cast spell");
        Ray currentRay = playerController.PlayerAvatarController.PlayerCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        RaycastHit hit;

        Projectile currentProjectile = PhotonNetwork.Instantiate(spellToCast, Camera.main.transform.position, Camera.main.transform.rotation).GetComponent<Projectile>();


        yield return null;
    }


    private void CastCheck(ref float manaSpent)
    {
        if(playerController.playerRescources.CurrentMana < manaSpent)
        {
            isSpecial = false;
            manaSpent = 0;
        }
    }

    public void SetCanCast(bool _can)
    {
        canCast = _can;
    }

    private void ArtifactCast()
    {
        playerController.PlayerAudioSource.PlayOneShot(SoundManager.Instance.SFX_Artifact_Activation);
        StartCoroutine(equipedArtifact.ArtifactEffect(this.gameObject));
    }

    private void ChangeArtifact(int delta)
    {
        equipeedArtifactIndex += delta;
        if(equipeedArtifactIndex >= artifactList.Count)
        {
            equipeedArtifactIndex = 0;
        }
        else if(equipeedArtifactIndex < 0)
        {
            equipeedArtifactIndex = artifactList.Count - 1;
        }

        ArtifactEquip();
    }

    private void ChangeArtifact()
    {
        foreach(KeyCode key in Enum.GetValues(typeof(KeyCode)))
        {
            if (Input.GetKeyDown(key))
            {
                foreach (var artifactInput in InputManager.Instance.ArtifactInputList)
                {
                    if(key == artifactInput.ArtifactKeycode)
                    {
                        equipeedArtifactIndex = artifactInput.ArtifactIndex;
                        ArtifactEquip();
                        isSpecial = false;
                        return;
                    }
                }
            }
        }
    }

    private void ArtifactEquip()
    {
        equipedArtifact = artifactList[equipeedArtifactIndex];
        HudManager.instance.CahngeArtifact(equipedArtifact.ArtifactSprite);
    }

    public void setSpecialAttack()
    {
        isSpecial = !isSpecial;
        
        if(isSpecial)
        {
            Debug.LogError("set special");
        }
        else
        {
            Debug.LogError("unset Special");
        }
    }

    private void CastSpell_SFX()
    {
        RpcCastSpell_SFX();
    }
    
    [PunRPC]
    private void RpcCastSpell_SFX()
    {
        playerController.PlayerAudioSource.PlayOneShot(SoundManager.Instance.SFX_Basoc_Cast);
    }
}
